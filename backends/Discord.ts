import {Backend, BackendConstructor} from "../lib/Backend";
import {executeString, Command, Context} from "../lib/Command";
import * as DiscordJS from 'discord.js';

export class Discord implements Backend {
    private client;
    
    commands: Command[] = [];
    
    start() {
        this.client = new DiscordJS.Client();
        this.init();
        console.log("Discord started");
    }
    
    send(msg: string, ctx: Context): void {
        this.client.channels.get(ctx.id).send(msg);
    }
    
    private init() {
        this.client.on('ready', () => {
            console.log('Discord ready')
        });
        
        this.client.on('message', msg => {
            if (msg.content.startsWith('$')) {
                msg.reply(executeString(msg.content, this, msg.channel.id));
            }
        });
        
        this.client.login(process.env.DISCORD_TOKEN);
    }
    
    constructor() {}
};