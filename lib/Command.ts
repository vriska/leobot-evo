import {Backend} from './Backend';

export class Arguments {
    readonly args: string;
    readonly backend: Backend;
    readonly self: boolean;
    
    constructor(args: string, backend: Backend, self: boolean) {
        Object.assign(this, {args, backend, self});
    }
}

export interface MessageSender {
    (msg: string, ctx: Context): void
}

export class Context {
    private args: Arguments;
    private sender: MessageSender;
    readonly id: number | null;
    
    constructor(args: Arguments, sender: MessageSender, id: number | null) {
        Object.assign(this, {args, sender, id});
    }
    
    reply(msg: string) {
        this.sender(msg, this);
    }
}

export interface Command {
    readonly isIdempotent: boolean;
    execute(args: Arguments, context: Context): void;
    readonly aliases: string[];
}

export interface CommandConstructor {
    new(): Command;
}

export function executeString(cmdline: string, backend: Backend, id: number | null = null) {
    let isSelf: boolean = false;
    
    if (cmdline.startsWith('#')) {
        isSelf = true;
    }
    
    let splitCmdline = cmdline.replace(/^\# ?|^\$ ?/, '').split(' ');
    let cmdName = splitCmdline.shift();
    let cmdArgs = splitCmdline.join(' ');
    
    let args = new Arguments(cmdArgs, backend, isSelf);
    
    let executed: boolean = false;
    
    let ctx: Context;
    
    let reply = '';
    let sender: MessageSender = msg => {
        if (executed) {
            backend.send(msg, ctx);
        } else {
            reply += `${msg}\n\n`;
        }
    };
    
    ctx = new Context(args, sender, id);
    
    let cmd = backend.commands.filter(e => e.aliases.includes(cmdName))[0];
    
    if (typeof(cmd) === 'undefined') {
        return 'Command not found.';
    }
    
    cmd.execute(args, ctx);
    
    executed = true;
    
    return reply.replace(/\n\n$/, '');
}

export function stringParse(argsStr: string) {
    return function(yargs) {
        let args = yargs.parse(argsStr);
        
        if (argsStr.search(/ ?-- /) != -1) {
            args._ = argsStr.replace(/^.*?-- /, '');
        } else {
            args._ = argsStr.replace(/^(?:-[^ ]+ )*/, '');
        }
        
        return args;
    };
}