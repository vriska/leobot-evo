const gulp = require('gulp');
const ts = require('gulp-typescript');
const newer = require('gulp-newer');
const spawn = require('child_process').spawn;
const babel = require('gulp-babel');
const revertPath = require('gulp-revert-path');

let node;

let tsProject = ts.createProject('tsconfig.json');

gulp.task('default', function() {
    return gulp.src(['./**/*.ts', '!./node_modules/**/*'])
        .pipe(newer('./dist'))
        .pipe(babel(require('./package.json').babel))
        .pipe(revertPath())
        .pipe(tsProject())
        .pipe(gulp.dest('./dist'))
});

gulp.task('run', ['default'], () => {
    if (node) node.kill()
    node = spawn('npm', ['run', 'start-built'], {stdio: 'inherit'})
    node.on('close', function (code) {
        if (code === 8) {
            gulp.log('Error detected, waiting for changes...');
        }
    });
});

gulp.task('watch', ['run'], function() {
    let watcher = gulp.watch(['./**/*.ts', '!./node_modules/**/*'], ['run']);

    watcher.on('change', () => {console.log('Updated, running...')});
});