import {Arguments, Context, Command, stringParse} from "../lib/Command";
import {encode as bent} from 'lunicode-bent';
import {encode as creepify} from 'lunicode-creepify';
import {encode as bubble} from 'lunicode-bubbles';
import * as randomCase from 'random-case';
import * as yargs from 'yargs';
import * as randomatic from 'randomatic';
import execall from 'execall2';

class RepeatingZalgoCommand implements Command {
    constructor() {}
    
    private inst(args, ctx: Context) {
        let orig = args._;
        
        if (args.garbage) {
            orig = orig.replace(/@/g, randomatic('?', Math.ceil(Math.random()*15)+10, {chars: '!\"#$%&\'()*+,-.\/0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\\]^_`abcdefghijklmnopqrstuvwxyz{|}~\u00A0\u00A1\u00A2\u00A3\u00A4\u00A5\u00A6\u00A7\u00A8\u00A9\u00AA\u00AB\u00AC\u00AD\u00AE\u00AF\u00B0\u00B1\u00B2\u00B3\u00B4\u00B5\u00B6\u00B7\u00B8\u00B9\u00BA\u00BB\u00BC\u00BD\u00BE\u00BF\u00C0\u00C1\u00C2\u00C3\u00C4\u00C5\u00C6\u00C7\u00C8\u00C9\u00CA\u00CB\u00CC\u00CD\u00CE\u00CF\u00D0\u00D1\u00D2\u00D3\u00D4\u00D5\u00D6\u00D7\u00D8\u00D9\u00DA\u00DB\u00DC\u00DD\u00DE\u00DF\u00E0\u00E1\u00E2\u00E3\u00E4\u00E5\u00E6\u00E7\u00E8\u00E9\u00EA\u00EB\u00EC\u00ED\u00EE\u00EF\u00F0\u00F1\u00F2\u00F3\u00F4\u00F5\u00F6\u00F7\u00F8\u00F9\u00FA\u00FB\u00FC\u00FD\u00FE\u00FF'}).replace(/\$/g, '$$$$'));
        }
        
        let origWords = (args.garbage || !orig.includes('@')) ? orig.split(/([-!"#$%&'()*+,./:;<=>?@\[\]^_`{ | }~])/g) : execall(/@(.*?)@/g, orig).map(e => e[1]);
        
        let words = (args.garbage || !orig.includes('@')) ? [...origWords] : execall(/@(.*?)@/g, orig).map(e => e[1]);
        
        if (args.r || args.R) {
            for (let i = 0; i < Math.floor(Math.random()*20) + 15; i++) {
                words = [...words, ...origWords, ' '];
            }    
        }
        
        let zalgoWords = words.map(e => {
            let manipulated: string = e;
            
            let surround = (args.bubble && Math.random() >= 0.8) || args['bubble-only'];
            let bend = !(args.basic || surround) && Math.random() >= 0.8;
            let caps = !(args.basic || surround) && (Math.random() >= 0.7);
            let zalgo = !surround && (args.basic || Math.random() <= 0.9 || !(bend || caps));
            
            if (caps) {
                manipulated = randomCase(manipulated);
            }
            
            if (bend) {
                manipulated = bent(manipulated);
            }
            
            if (surround) {
                manipulated = bubble(manipulated);
            }
            
            if (zalgo) {
                manipulated = creepify(manipulated);
            }
            
            return manipulated;
        });
        
        if (!args.garbage && orig.includes('@')) {
            let indexes = execall(/@(.*?)@/g, orig).map(e => [e.index, e.index + e[0].length]);
            
            ctx.reply(indexes.reduce((acc, cur, ind) => 
                    acc + zalgoWords[ind] + orig.slice(indexes[ind][1], ind === indexes.length - 1 ? orig.length - 1 : indexes[ind + 1][0]), 
                orig.slice(0, indexes[0][0])));
        } else {
            ctx.reply(zalgoWords.join(''));
        }
    }
    
    private repetition(counter: number, args, ctx: Context) {
        setTimeout(() => {
            this.inst(args, ctx);
            
            counter--;
            
            if (counter > 0) {
                this.repetition(counter, args, ctx);
            }
        }, 100 + (Math.random() * 750));
    }
    
    execute(argsObj: Arguments, ctx: Context) {
        let args = yargs
                        .boolean('r')
                        .boolean('R')
                        .boolean('bubble')
                        .boolean('bubble-only')
                        .boolean('basic')
                        .boolean('garbage')
                        .alias(Buffer.from('anVzdC1idWJibGlrYQ==', 'base64').toString('ascii'), 'bubble-only') // 👀
                        .string('_')
                        |> stringParse(argsObj.args);
        
        this.inst(args, ctx);
        
        if (!!args.r) {
            let repetitions = Math.ceil(Math.random() * 2) + 2;
        
            this.repetition(repetitions, args, ctx);
        }
    }
    
    readonly isIdempotent: boolean = true;
    readonly aliases: string[] = ["zalgo"];
}

export default RepeatingZalgoCommand;