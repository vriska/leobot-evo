import {Backend, BackendConstructor} from "./lib/Backend";
import {WebTester} from "./backends/WebTester";
import {TCPBackend} from './backends/TCPBackend';
import {Discord} from './backends/Discord';
import commands from './commands';

let Backends: BackendConstructor[] = [WebTester, TCPBackend, Discord];

let backends: Backend[] = Backends.map((backend: BackendConstructor) => new backend());

backends.forEach(e => {e.commands = commands});

backends.forEach(e => e.start());
